﻿#pragma once

class GameStructure // the class
{
	public: // public
		 virtual void Help() = 0; // this is a pure virtual method
};
