#include "Ninja.h" // allows access to the ninja class
#include "iostream" // allows input and output

int Ninja::ThrowStars()
{
	std::cout << name << " throws some shuriken and deals " << damage << " damage! \n"; // outputs text
	return damage; // returns the damage
}

Ninja::Ninja()
{
	std::cout << "What is the ninja's name? \n"; // outputs text
	std::getline(std::cin, name); // gets the user input
}

int Ninja::Attack()
{
	std::cout << name << " did 25 damage \n"; // outputs text
	return 25; // returns the damage
}

void Ninja::Help()
{
	std::cout << "If you are having trouble try again! The values are randomized you might have just gotten unlucky. \n"; // outputs text
	std::cout << "Ninja might be better next time! \n"; // outputs text
}

void Ninja::Start()
{
	std::cout << "So you picked the ninja. \n"; // outputs text
	std::cout << "That means that you'll be fighting the pirate!  \n"; // outputs text
}

int Ninja::Fight()
{
	int choice; // a int to store the choice
	std::string placeHolderChoice; // a string to store the choice placeholder
	bool picked; // a bool to check if an attack was picked
	
	do
	{
		try
		{
			std::cout << "What would you like to do? \n"; // outputs text
			std::cout << "1.) Throw shuriken! \n"; // outputs text
			std::cout << "2.) Attack! \n"; // outputs text
			std::cout << "3.) Help? \n"; // outputs text


			std::cin >> placeHolderChoice; // gets the user input
			choice = std::stoi(placeHolderChoice); // changes the string into an int

			if (choice != 1 && choice != 2 && choice != 3)
			{
				choice = std::stoi("Error"); // forces an error
			}

			if (choice == 3)
			{
				Help(); // outputs text
				choice = std::stoi("Error"); // forces an error
			}
			
			picked = true; // sets picked to true
			return choice; // returns the choice
		}
		catch (...)
		{
			std::cout << "Please type either 1, 2 or 3 \n"; // outputs text
			picked = false; // sets picked to false
		}
	} while (picked == false); // continues until picked equals true
}


