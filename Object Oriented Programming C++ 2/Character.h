﻿#pragma once
#include "GameStructure.h" // allows access to the game structure class
#include <string> // allows the use of string

class Character : public GameStructure //character inherits from game structure
{
	public: // public
	std::string name; // a string to store a name
		int damage; // a int to store the damage value
		bool dead = false; // a bool to store if dead
		
		int GetHealth(); // gets the health
		void SetHealth(int healthSet); // sets the health
		int DisplayStats(int attackDamage); // displays the stats
		void Talk(std::string stuffToSay); // outputs text
		void Talk(std::string stuffToSay, std::string name); // outputs text
		virtual int Attack(); // holds a default value for attacking
		void Help() override; // holds a default value for help

	private: // private
		int health = 0; // a int to store health
};
