#include "Character.h" // allows access to the character class
#include "GameDriver.h" // allows access to the game driver class
#include "Ninja.h" // allows access to the ninja class
#include "Pirate.h" // allows access to the pirate class
#include <iostream> // allows input and output

void main()
{
	int pickedRole; // a int to store the picked role
	int pickedChoice; // a int to store the picked choice
	int tempHealth; // a int to store health temporarily
	int tempDamage; // a int to store damage temporarily
	
	GameDriver gameDriver; // constructs a game driver

	gameDriver.Intro(); // outputs text
	
	Ninja ninja; // constructs a ninja
	ninja.SetHealth(gameDriver.RandomRoll()); // sets the health to a random number
	ninja.damage = ninja.DisplayStats(gameDriver.StatRoll()); // sets the damage to a random number
	ninja.Talk("You should choose Ninja! \n"); // outputs text
	ninja.Talk("Ninja's are stealthy and dangerous! \n", ninja.name); // outputs text

	Pirate pirate; // constructs a pirate
	pirate.SetHealth(gameDriver.StatRoll()); // sets the health to a random number
	pirate.damage = pirate.DisplayStats(gameDriver.StatRoll()); // sets the damage to a random number
	pirate.Talk("You should choose Pirate! \n"); // outputs text
	pirate.Talk("Pirates are load and bold! \n", pirate.name); // outputs text
	
	pickedRole = gameDriver.Choose(); // makes the user choose
	if (pickedRole == 1) // ninja
	{
		ninja.Start(); // outputs text
		
		do
		{
			pickedChoice = ninja.Fight(); // has the user choose what attack

			if (pickedChoice == 1) // special
			{
				tempDamage = ninja.ThrowStars(); // gets the damage of the attack
				tempHealth = pirate.GetHealth(); // gets the targets health
				tempHealth -= tempDamage; // deals the damage
				pirate.SetHealth(tempHealth); // updates the health
				
				if (pirate.dead == false)
				{
					pirate.DisplayStats(pirate.damage); // displays the targets health
					tempDamage = pirate.UseSword(); // gets the damage of the attack
					tempHealth = ninja.GetHealth(); // gets the targets health
					tempHealth -= tempDamage; // deals the damage
					ninja.SetHealth(tempHealth); // updates the health
					
					if (ninja.dead == false)
					{
						ninja.DisplayStats(ninja.damage); // displays the targets health
					}
				}
			}
			else if (pickedChoice == 2) // attack
			{
				tempDamage = ninja.Attack(); // gets the damage of the attack
				tempHealth = pirate.GetHealth(); // gets the targets health
				tempHealth -= tempDamage; // deals the damage
				pirate.SetHealth(tempHealth); // updates the health
				
				if (pirate.dead == false)
				{
					pirate.DisplayStats(pirate.damage); // displays the targets health
					tempDamage = pirate.UseSword(); // gets the damage of the attack
					tempHealth = ninja.GetHealth(); // gets the targets health
					tempHealth -= tempDamage; // deals the damage
					ninja.SetHealth(tempHealth); // updates the health
					
					if (ninja.dead == false)
					{
						ninja.DisplayStats(ninja.damage); // displays the targets health
					}
				}
			}
		} while (ninja.dead == false && pirate.dead == false); // continues while both are alive

		if (pirate.dead == true)
		{
			std::cout << "You Win!"; // outputs text
		}
		if (ninja.dead == true)
		{
			std::cout << "You Lost"; // outputs text
		}
	}
	else if (pickedRole == 2) // pirate
	{
		pirate.Start(); //outputs text
		
		do
		{
			pickedChoice = pirate.Fight(); // has the user choose what attack

			if (pickedChoice == 1) // special
			{
				tempDamage = pirate.UseSword(); // gets the damage of the attack
				tempHealth = ninja.GetHealth(); // gets the targets health
				tempHealth -= tempDamage; // deals the damage
				ninja.SetHealth(tempHealth); // updates the health
				
				if (ninja.dead == false)
				{
					ninja.DisplayStats(ninja.damage); // displays the targets health
					tempDamage = ninja.ThrowStars(); // gets the damage of the attack
					tempHealth = pirate.GetHealth(); // gets the targets health
					tempHealth -= tempDamage; // deals the damage
					pirate.SetHealth(tempHealth); // updates the health
					
					if (pirate.dead == false)
					{
						pirate.DisplayStats(pirate.damage); // displays the targets health
					}
				}
			}
			else if (pickedChoice == 2) // attack
			{
				tempDamage = pirate.Attack(); // gets the damage of the attack
				tempHealth = ninja.GetHealth(); // gets the targets health
				tempHealth -= tempDamage; // deals the damage
				ninja.SetHealth(tempHealth); // updates the health
				
				if (ninja.dead == false)
				{
					ninja.DisplayStats(ninja.damage); // displays the targets health
					tempDamage = ninja.ThrowStars(); // gets the damage of the attack
					tempHealth = pirate.GetHealth(); // gets the targets health
					tempHealth -= tempDamage; // deals the damage
					pirate.SetHealth(tempHealth); // updates the health
					
					if (pirate.dead == false)
					{
						pirate.DisplayStats(pirate.damage); // displays the targets health
					}
				}
			}
		} while (pirate.dead == false && ninja.dead == false); // continues while both are alive

		if (pirate.dead == true)
		{
			std::cout << "You Lost"; // outputs text
		}
		if (ninja.dead == true)
		{
			std::cout << "You Win!"; // outputs text
		}
	}
}