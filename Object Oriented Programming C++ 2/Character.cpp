﻿#include "Character.h" // allows access to the character class
#include <iostream> // allows out put and input

int Character::GetHealth()
{
	return health; // returns health
}

void Character::SetHealth(int healthSet)
{
	if (healthSet <= 0) // if the health is 0 or below
	{
		healthSet = 0; // set health to 0
		dead = true; // dead equals true
		std::cout << name << " has Expired... \n"; // outputs text
	}
	health = healthSet; // updates health
}

int Character::DisplayStats(int attackDamage)
{
	std::cout << name << ", has " << GetHealth() << " Health and does " << attackDamage << " Damage. \n"; // outputs text

	return attackDamage; // returns the damage
}

void Character::Talk(std::string stuffToSay)
{
	std::cout << name << " says " << stuffToSay; // outputs text
}

void Character::Talk(std::string stuffToSay, std::string name)
{
	std::cout << name << " says " << stuffToSay; // outputs text
}

int Character::Attack()
{
	return 10; // returns damage
}

void Character::Help()
{
	// In the Character class, the Help method would be overridden and empty.
}