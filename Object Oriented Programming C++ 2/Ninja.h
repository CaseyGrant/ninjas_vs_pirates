#pragma once
#include "Character.h" // allows access to the character class
class Ninja : public Character // ninja inherits from the character class
{
	public: // public
		int ThrowStars(); // returns random damage value
		Ninja(); // constructor for ninja
		int Attack() override; // returns damage value
		void Help() override; // outputs text
		void Start(); // outputs text
		int Fight(); // picks an attack
};

