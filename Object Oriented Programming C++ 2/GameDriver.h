﻿#pragma once

class GameDriver // the class
{
	public: // public
		void Intro(); // outputs text
		int RandomRoll(); // generates a random number between 1 and 100
		int StatRoll(); // generates a random number between 1 and 25
		int Choose(); // picks between pirate and ninja
};
