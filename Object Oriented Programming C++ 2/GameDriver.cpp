﻿#include "GameDriver.h" // allows access to the game driver class
#include <iostream> // allows input and output
#include <ctime> // allows the use of time
#include <string> // allows the use of string

void GameDriver::Intro()
{
	std::cout << "Welcome to the character generator that lets you get right into the action! \n"; // outputs text
}

int GameDriver::RandomRoll()
{
	//For extra credit add a method called RandomRoll to the GameDriver class which returns a number > 0 and <101.
	srand(time(NULL)); // sets the random seed
	int number = rand() % 100 + 1; // gets a random number between 1 and 100
	
	return number; // returns the number
}

int GameDriver::StatRoll()
{
	int number = rand() % 25 + 1; // gets a random number between 1 and 25

	return number; // returns the number
}

int GameDriver::Choose()
{
	int role; // a int to store the role
	std::string placeHolderRole; // a string to hold the place of the role
	bool picked; // a bool to store if a role has been picked
	do
	{
		try
		{
			std::cout << "Would you like to play as a Pirate or a Ninja? \n"; // outputs text
			std::cout << "1.) Ninja! \n"; // outputs text
			std::cout << "2.) Pirate! \n"; // outputs text
			

			std::cin >> placeHolderRole; // gets the user input
			role = std::stoi(placeHolderRole); // turns the string into a number

			if (role != 1 && role != 2)
			{
				role = std::stoi("Error"); // forces an error
			}
			picked = true; // sets picked to true
			return role; // returns the role
		}
		catch (...)
		{
			std::cout << "Please type either 1 or 2 \n"; // outputs text
			picked = false; // sets picked to false
		}
	}
	while (picked == false); // continues until picked equals true
}