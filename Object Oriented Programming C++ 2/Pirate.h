#pragma once
#include "Character.h" // allows access to the character class
class Pirate : public Character // pirate inherits from the character class
{
	public:
		int UseSword(); // returns random damage value
		Pirate(); // constructor for pirate
		int Attack() override; // returns damage value
		void Help() override; // outputs text
		void Start(); // outputs text
		int Fight(); // picks an attack
};

